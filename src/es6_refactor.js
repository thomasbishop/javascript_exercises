function determinePositive(...arr)
{
    const postiveIntegers = [];
    for (const element of arr){
        if (element >= 1)
        {
            postiveIntegers.push(element);
        }
    }
    return postiveIntegers;
}

const output = determinePositive(4, 3, -2, 98, -0.2);
//console.log(output); // 4, 3, 98

function foo(){
    var a = 2;
    function bar(){
        console.log(a);
    }
   return bar;
}

//var baz = foo();
//baz();
//bar();
//foo();
//console.log(a); // Reference error 
//console.log(foo());

// This function is just a wrapper for calling any function we pass to it
function bar(fn) {
	fn();
}

// This function contains the data we want to be able to access outside of its lexical scope
function foo(){
	var a = 2; // our old friend
	
	function baz(){
			console.log(a);
	}

	bar(baz); // we use the bar function in global scope, so that foo always outputs baz
}

function foo2(){
    var four = 4;
    return four;
}

function foo3(passed){
    return passed * 2;
}

foo();
foo2();
foo3(4);