const arr = [5, 3, 2];
const initialValue = 0;
console.log(arr); 

console.log('Initial value was ' + initialValue);
const reduceFunc = function(accumulator, reducer, currentIndex, array)
{
    
    console.log('Accumulator = ' + accumulator, 'Reducer = ' + reducer, 'Index = ' + currentIndex + ', Array value is ' + array[currentIndex] );
    return accumulator + reducer;
}

const sum = arr.reduce(reduceFunc, initialValue);
console.log(sum);