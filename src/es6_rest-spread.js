function demoFunc(a, b, ...moreArgs) {
    console.log('1: ', a);
    console.log('2: ', b);
    console.log('more: ', moreArgs);
    console.log(moreArgs[3]);
}

// demoFunc('one', 'two', 'three', 'four', 'five', 'six');

function sum(x, y, z) {
    return x + y + z;
}

const numbers = [1, 2, 3];

//console.log(sum(...numbers));