var tableBody1 = document.querySelector(".table-body-1");
var tableBody2 = document.querySelector(".table-body-2");
/* XML AJAX starts */

var xmlAJAX = new XMLHttpRequest();


function getNodeValue(obj, tag) {                   // Gets content from XML
    return obj.getElementsByTagName(tag)[0].firstChild.nodeValue;
}

function createCellsXml(target, nodeName, appendTo) {
    var cell = document.createElement("td");
    cell.innerHTML = getNodeValue(target, nodeName);
    appendTo.appendChild(cell);
}

xmlAJAX.onload = function () {
    var data = xmlAJAX.responseXML;
    var fruits = data.getElementsByTagName("fruit");
    for (let i = 0; i < fruits.length; i++) {
        let row = document.createElement("tr");
        createCellsXml(fruits[i], 'name', row);
        createCellsXml(fruits[i], 'colour', row);
        createCellsXml(fruits[i], 'weight', row);
        createCellsXml(fruits[i], 'taste', row);
        tableBody1.appendChild(row)
   }
}

xmlAJAX.open('GET', '/ajax/data/data.xml', false);
xmlAJAX.send(null);

/* XML AJAX ends */

/* JSON AJAX starts */


var jsonAJAX = new XMLHttpRequest;

function createCellsJson(appendTo, targetValue)
{
  let cell = document.createElement("td");
  cell.innerHTML = targetValue;
  appendTo.appendChild(cell);
}

jsonAJAX.onload = function () {
  var data = JSON.parse(jsonAJAX.responseText); // create JS object from fetched JSON
  var fruits = data.fruits.fruit;

  for (let i = 0; i < fruits.length; i++ ) {
    let row = document.createElement("tr");
    createCellsJson(row, fruits[i].name);
    createCellsJson(row, fruits[i].colour);
    createCellsJson(row, fruits[i].weight);
    createCellsJson(row, fruits[i].taste);
    tableBody2.appendChild(row);
  }
}

jsonAJAX.open('GET', '/ajax/data/data.json', true);
jsonAJAX.send(null);
/* JSON AJAX ends */


let aNumber = 11;
let aBoolean = true;
let aString = "this sentence";

console.log(`It is ${aBoolean} that ${aString} has ${aNumber} words in it `);
